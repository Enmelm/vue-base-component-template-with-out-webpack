export default {
    name: 'App',
    template: `
      <div class="base-grid">
        <div class="fluid ui secondary vertical pointing menu">
          <router-link to="/advertising" class="active item">Advertising
          </router-link>
          <router-link to="/product" class="item">Productos
          </router-link>
        </div>
        <router-view></router-view>
      </div>
    `,
  };
