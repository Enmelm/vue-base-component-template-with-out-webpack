export default {
    name: 'Modal',
    props: ['advertise'],
    data: function () {
        return {
                isLoad: false,
            }
        },
    template: `
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">

                    <div class="modal-header">
                        <slot name="header">
                        default header
                        </slot>
                    </div>

                    <div class="modal-body">
                        <slot name="body">
                        default body
                        </slot>
                    </div>

                    <div class="modal-footer">
                        <slot name="footer">
                            <div class="ui row">
                                <button class="ui icon red button" @click="$emit('close')">
                                    <i class="remove icon"></i>
                                </button>
                                <button class="ui icon green button" @click="showModal = true">
                                    <i class="checkmark icon"></i>
                                </button>
                            </div>
                        </slot>
                    </div>
                </div>
            </div>
        </div>
    </transition>
    `,
}