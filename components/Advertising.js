import User from "../backend/login.js";
import Http from "../backend/http.js";
import ImageBox from "./ImageBox.js";

export default { 
    name: 'Advertise',
    props: ['data'],
    components: {
      ImageBox
    },
    template: `
    <div class="item">
      <ImageBox :url=data.files.pictures[0].url></ImageBox>
      <div class="content">
        <div class="header">{{data.name}} <div class="ui red horizontal label advertising-label">{{data.type}}</div> </div>
        {{data.description}}
      </div>
      <div class="right floated content">
        <button class="ui icon button" @click="deleteThis">
          <i class="trash alternate icon"></i>
        </button>
        <button class="ui icon button">
          <i class="pencil alternate icon"></i>
        </button>
      </div>
    </div>
    `,
    methods: {
      deleteThis(){
        var http = new Http();
        http.deleteAdvertise(this.data.advertising_key).then((result) => {
          if(result.status == "200"){
            /* this.$parent.$emit('delete_row', this.data.advertising_key); */
            bus.$emit('delete_row', this.data.advertising_key);
          }
        });
      }
    }
  };