export default {
    name: 'ImageBox',
    props: ['url'],
    data: function () {
        return {
                isLoad: false,
            }
        },
    methods: {
        loadImg() {
            this.isLoad = false
            },
            loaded() {
            this.isLoad = true
            }
        },
    created: function() {
        this.loadImg()
        },
    template: `
        <div class="left floated content">
        <transition>
            <img class="advertisin-img_container" v-show="isLoad" :src="url" @load="loaded">
        </transition>
        <div v-show="!isLoad" class="ui active inline loader"></div>
        </div>
    `,
}