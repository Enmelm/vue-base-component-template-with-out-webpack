import User from "../backend/login.js";
import Http from "../backend/http.js";

export default {
    name: 'FormLogin',
    template: `
    <div class="ui inverted segment">
        <div class="ui inverted form">
            <div class="field">
                <label>Email</label>
                <input placeholder="example@example.com" type="text" v-model="email">
            </div>
            <div class="field">
                <label>Password</label>
                <input placeholder="secret123" type="password" v-model="password">
            </div>
            </div>
            <div class="inline field login-line">
                <div class="ui checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label>Recuerdame</label>
                </div>
            <div class="ui submit button" v-on:click='login' >Log in</div>
        </div>
    </div>
    `,
    mounted: function () {
        this.$nextTick(function () {
            $('.ui.checkbox').checkbox();
        })
    },

    data: function () {
        return {
            email: '',
            password: '',
        }
    },

    methods: {
        login() {
            var user = new User(this.email, this.password);
            var http = new Http();
            http.login(user).then((result) => {
                if (result) {
                    this.$router.push("/home");
                }
            });
        },
    },

};