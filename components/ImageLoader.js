export default {
    name: 'ImageLoader',
    data: function () {
        return {
            file: '',
            showPreview: false,
            imagePreview: ''
          }
        },
    methods: {
        handleFileUpload(){

            this.file = this.$refs.file.files[0];
 
            let reader  = new FileReader();
        
            reader.addEventListener("load", function () {
              this.showPreview = true;
              this.imagePreview = reader.result;
            }.bind(this), false);
        
            if( this.file ){

              if ( /\.(jpe?g|png|gif)$/i.test( this.file.name ) ) {
                reader.readAsDataURL( this.file );
              }
            }
          }
        },

    created: function() {
        },
    template: `
        <div class="container">
        <div class="large-12 medium-12 small-12 cell">
        <label>File Preview
            <input type="file" id="file" ref="file" accept="image/*" v-on:change="handleFileUpload()"/>
        </label>
        <img v-bind:src="imagePreview" v-show="showPreview"/>
        </div>
    </div>
    `,
}