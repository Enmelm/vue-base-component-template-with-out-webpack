import Advertise from './../components/Advertising.js';
import Http from "../backend/http.js";
import Advertising from './../components/Advertising.js';
import Modal from './../components/Modal.js';

export default {
    name: 'AdvertisingView',
    components:  {
        Advertise,
        Modal
        },
    template: `
    <div>
        <div class="ui row">
            <button class="ui icon button" v-on:click='fetchAdvertise'>
                <i class="redo icon"></i>
            </button>
            <button class="ui icon button" @click="showModal = true">
                <i class="plus icon"></i>
            </button>
        </div>
        <transition-group name="list" tag="div" class="ui middle aligned divided list overflowlist" @delete_raw="deleteRow">
            <Advertise v-for="advertise in advertises" :key="advertise.advertising_key" :data="advertise"></Advertise>
        </transition-group>

        <modal v-if="showModal" @close="showModal = false">
        </modal>
    </div>
    `,
    
    data: function () {
        return {
                advertises: [],
                showModal: false,
            }
        },

    mounted: function () {

        var http = new Http();

        http.fetchAdvertises().then((result) => {
            this.advertises = result.data.data;
            console.log(this.advertises);
        });

        bus.$on('delete_row', (advertising_key) => {
            let index = this.advertises.findIndex((element) => {
                return element.advertising_key == advertising_key;
            });

            this.advertises.splice(index, 1);
        });
    },
        

    methods: {

        fetchAdvertise () {
            var http = new Http();
            http.fetchAdvertises().then((result) => {
                this.advertises = result.data.data;
            });
        },
        deleteRow: function(advertising_key) {
            let index = this.advertises.findIndex((element) => {
                return element.advertising_key == advertising_key;
            });

            this.advertises.splice(index, 1);
        },
    },
  };