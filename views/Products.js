import Http from "../backend/http.js";
import ImageLoader from "../components/ImageLoader.js";

export default {
    name: 'ProductsView',
    components: {

    },
    template: `

    <div class="products-layout">

    <form class="ui form product-info">
        <h4 class="ui dividing header">Information del producto</h4>
        <div class="field">
            <label>Nombre</label>
            <input type="text" v-model="name">
        </div>

        <div class="two fields">
            <div class="field">
                <label>Categoria</label>
                <div class="ui fluid search selection dropdown">
                    <input type="hidden" name="category">
                    <i class="dropdown icon"></i>
                    <div class="default text">Seleccione categoria</div>
                    <div class="menu">
                        <div v-for="category in categories" class="item" :value="category.category_key" :key="category.id"><i class="af flag"></i>{{category.name}}</div>
                    </div>
                </div>
            </div>
            <div class="field">
                <label>Uso</label>
                <select id="status" v-model="status" class="ui fluid dropdown">
                    <option value="">Seleccione estado</option>
                    <option :value='0'>Nuevo</option>
                    <option :value='1'>Seminuevo</option>
                    <option :value='2'>Semiusado</option>
                    <option :value='3'>Usado</option>
                </select>
            </div>
        </div>

        <div class="two fields">
            <div class="field">
                <label>Precio</label>
                <div class="ui right labeled input">
                    <input type="text" v-model="price" placeholder="1,00" id="amount">
                    <label for="amount" class="ui label">$</label>
                </div>
            </div>
            <div class="field">
                <label>Cantidad</label>
                <input type="text" v-model="amount">
            </div>
        </div>

        <div class="field">
            <label>Descripción</label>
            <textarea id="description" v-model="description"></textarea>
        </div>

    <div class="ui accordion">
        <div class="title">
            <i class="dropdown icon"></i>
            <span class="ui dividing header">Shipping Information</span>
        </div>
        <div class="content">
            <div class="two fields">
                <div class="field">
                    <label>Precio Nacional</label>
                    <div class="ui right labeled input">
                        <input type="text" v-model="nacional" placeholder="1,00" id="amount">
                        <label for="amount" class="ui label">$</label>
                    </div>
                </div>
                <div class="field">
                    <label>Precio Regional</label>
                    <div class="ui right labeled input">
                        <input type="text" v-model="regional" placeholder="1,00">
                        <label for="amount" class="ui label">$</label>
                    </div>
                </div>
            </div>
            <div class="field">
                    <label>Dirección</label>
                    <textarea rows="2"></textarea>
            </div>
        </div>
    </div>
    <div class="fluid ui button" id="publicar-btn" @click="postProduct()" tabindex="0">Peticion de publicacion</div>
</form>
    <div class="files-layout">
        <div  v-for="(image, i) in files"  class="imgwrapper">
            <img class="ui image imagen-product" :src="image.url" @click="deleteImage(i)">
            <div class="image-extra">
                    <p>Filename: {{image.name}}</p>
                    <p>Size: {{image.size}}.bytes</p>
            </div>
        </div>

        <div v-if="files.length < 5" class="image-upload">
            <label for="file-input">
                <i class="camera icon"></i>
            </label>
            <input type="file" id=file-input @change="saveFile($event)" ref="fileInput">
        </div>
    </div>

</div>
    `,

    data: function () {
        return {
            name: '',
            description: '',
            price: '',
            use: '',
            selected_category: '',
            direction: '',
            status: '',
            amount: '',
            files: [],
            categories: [],
            nacional: 0,
            regional: 0,
        }
    },

    mounted: function () {

        $('.ui.dropdown').dropdown();

        $('.ui.accordion').accordion();

        var http = new Http();

        http.getAllCategories().then((categories) => {
            this.categories = categories;
        });
    },


    methods: {

        deleteImage(i) {
            this.files.splice(i, 1);
        },

        getBase64(file) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.readAsBinaryString(file);
                reader.onload = () => {
                    return resolve(reader.result)
                };
                reader.onerror = error => reject(error);
            });
        },

        getUrl(file) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => {
                    return resolve(reader.result)
                };
                reader.onerror = error => reject(error);
            });
        },

        saveFile(event) {
            let file = event.target.files[0];
            this.getBase64(file).then((dataStream) => {
                let base64_file = dataStream;
                this.getUrl(file).then((dataUrl) => {
                    this.files.push({
                        url: dataUrl,
                        stream: base64_file,
                        size: file.size,
                        name: file.name,
                        mime: file.type
                    });
                    this.$refs.fileInput.value = '';
                });
            });
        },

        postProduct() {

            var sessionToken = localStorage.getItem('id_token');

            let file0;
            let file1;
            let file2;
            let file3;
            let file4;

            if (this.files[0]) {
                file0 = this.files[0].stream;
            }

            if (this.files[1]) {
                file1 = this.files[1].stream;
            }

            if (this.files[2]) {
                file2 = this.files[2].stream;
            }

            if (this.files[3]) {
                file3 = this.files[3].stream;
            }

            if (this.files[4]) {
                file4 = this.files[4].stream;
            }

            axios({
                    method: 'POST',
                    url: 'http://dev5.mydesk.digital/WebServiceCA/market/productbit',

                    data: {
                        name: this.name,
                        description: this.description,
                        price: this.price,
                        use: this.use,
                        status: this.status,
                        direction: this.direction,
                        category: this.category,
                        amount: this.amount,
                        file0: file0,
                        file1: file1,
                        file2: file2,
                        file3: file3,
                        file4: file4,
                    },

                    auth: {
                        username: 'test',
                        password: sessionToken
                    },
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .then(function (response) {
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error.response);
                });
        }
    },
};