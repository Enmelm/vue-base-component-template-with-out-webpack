import FormLogin from './../components/FormLogin.js';

export default {
    name: 'LoginView',
    components:  {
        FormLogin,
        },
    template: `
    <div class="login-form_center">
        <FormLogin></FormLogin>
    </div>
    `,
  };