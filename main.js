import App from './components/App.js';
import HomeView from './views/Home.js';
import LoginView from './views/Login.js';
import AdvertisingView from './views/Advertising.js';
import ProductsView from './views/Products.js';
import config from './config.js';


//tell vue to use the router

window.baseUrl = config.root_url;

Vue.use(VueRouter)
//define your routes
const routes = [

  { path: '/home', component: HomeView },
  { path: '/', component: LoginView },
  { path: '/advertising', component: AdvertisingView },
  { path: '/product', component: ProductsView },

]

const router = new VueRouter(
  {routes}
);

window.bus = new Vue({});

new Vue({
  router,
  render: h => h(App),
  data: {
    id_token: "",
  }
}).$mount(`#app`); 