export default class reQuestHero {

    login(user) {

        return axios({
                method: 'post',
                url: 'http://dev5.mydesk.digital/WebServiceCA/users/login',
                auth: {
                    username: 'test',
                    password: ''
                },
                data: {
                    password: user.password,
                    email: user.email,
                    application_id: '0a'
                }
            })

            .then(function (response) {

                let idToken = response.data.data.idToken;

                if (response.status == 200) {
                    localStorage.setItem('id_token', idToken);
                    return response;
                }
            })
            .catch(function (error) {
                return error;
            });

    }

    fetchAdvertises(type = "") {

        var sessionToken = localStorage.getItem('id_token');

        return axios({
                method: 'get',
                url: `http://dev5.mydesk.digital/WebServiceCA/advertising/${type}`,
                auth: {
                    username: 'test',
                    password: sessionToken
                },
            }).then(function (response) {

                return response;
            })
            .catch(function (error) {
                return error;
            });
    }

    deleteAdvertise(key) {

        var sessionToken = localStorage.getItem('id_token');

        return axios({
                method: 'post',
                url: `http://dev5.mydesk.digital/WebServiceCA/advertising/delete/${key}`,
                auth: {
                    username: 'testing',
                    password: sessionToken
                },
            }).then(function (response) {
                console.log(response);
                return response;
            })
            .catch(function (error) {
                return error;
            });
    }

    getAllCategories(option) {

        return axios({
                method: 'get',
                url: 'http://dev5.mydesk.digital/WebServiceES/market_r/categories/1',
                auth: {
                    username: 'test',
                },
            })
            .then(response => {
                return response.data.data;
            }).catch(function (error) {
                return error;
            });
    }

    getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsBinaryString(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    refresh() {
        alert('refrescado');
    }
};